import js2xml
import scrapy
import re


class amazonSpider(scrapy.Spider):
    name = 'amazonSpider'
    allowed_domains = ['amazon.in']
    start_urls = ['https://www.amazon.in/dp/B07HGJFSD8/']

    def parse(self, response):
        item = dict()
        js = response.xpath("//script[contains(text(), 'register(\"ImageBlockATF\"')]/text()").extract_first()
        xml = js2xml.parse(js)
        selector = scrapy.Selector(root=xml)
        title = response.xpath('//h1[@id="title"]/span/text()').extract()
        originalprice = response.xpath('//span[@id="priceblock_ourprice"]/text()').extract_first()
        availability = response.xpath('//div[@id="availability"]//text()').extract()
        category = response.xpath('//a[@class="a-link-normal a-color-tertiary"]/text()').extract()
        rating = response.css('i[data-hook="average-star-rating"]')
        rating = rating.css('span[class="a-icon-alt"]::text').extract_first()
        strikedprice = response.xpath(
            '//span[@class="priceBlockStrikePriceString a-text-strike"]/text()').extract_first()
        # sale_price = response.xpath('//span[contains(@id,"ourprice") or contains(@id,"saleprice")]/text()').extract()
        productdescription = response.xpath("//div[@id='featurebullets_feature_div']//span[@class='a-list-item']//text()").getall()

        item['image_urls'] = selector.xpath(
            '//property[@name="colorImages"]//property[@name="hiRes"]/string/text()').extract()
        item['title'] = ''.join(title).strip()

        item['strikedprice'] = strikedprice.replace(u'\xa0', u' ')

        item['originalprice'] = originalprice.replace(u'\xa0', u' ')
        # item['product_sale_price'] = ''.join(sale_price).strip()
        item['star-rating'] = float(rating.replace(" out of 5 stars", ""))
        item['rating'] = response.xpath('//span[@id="acrCustomerReviewText"]/text()').extract_first()
        item['description'] = response.xpath('//div[@id="feature-bullets"]//ul[@class="a-unordered-list"]').extract()
        product_description = []
        for description_temp in productdescription:
            product_description.append(description_temp.strip())
        item['product_description'] = product_description
        item['product_category'] = ','.join(map(lambda x: x.strip(), category)).strip()
        item['availability'] = ''.join(availability).strip()

        yield item
